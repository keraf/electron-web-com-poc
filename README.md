> This was part of a technical interview task. The instruction was to create a proof of concept of a third party website retrieving information from a locally running Electron application.

# Electron-Web Communication PoC
The goal of this proof of concept is to demonstrate how a third party website can access data from a locally running Electron application.

This PoC also has the goal to demonstrate how this can be securely achieved. This requires a first party API to authorize the third party website to access the local client's data. In addition of authorizing access, the first party API also provides information about the requester to the client in order for the user to approve the request. Additional security measures are suggested in the ___Improvements___ section.

## Running the PoC
Please refer to the README file of each project on how to run each project (spoiler: they all use the same command 😄). Configuration files are pre-populated with the correct values to "just work". 

Run all three projects in the same time (in no specific order). Open the [third party website](http://localhost:3002) and observe the electron app. A prompt will appear with the information of the requested. If approved, the key will be transmitted to the third party website. The deny flow was not implemented.

## Demo
[![Demo](https://img.youtube.com/vi/EyNWpP5hmpM/0.jpg)](https://www.youtube.com/watch?v=EyNWpP5hmpM "Demo")

## Architecture
We have three moving parts in this PoC:
- The third party website that wishes to request information from a local client
- The local client which contains the information we want
- The first party API to authorize access to the local client

### Third Party Website
When the third party website whishes to retrieve information from the local client, it first needs to get the permission to do this. The permission is granted via the first party API. Using an authenticated endpoint (using an API key), we identify who's requesting permission, and if successful, we provide a JWT with a short life time. 

This JWT is then used by the front end of the third party website to issue an HTTP request to the local client. I am using an event-stream request in order to keep it alive until the client responds to the authorization prompt without relying on long polling (multiple requests with timeouts) or web sockets (more complex implementation on the local client). If the user accepts the prompt, the requested data is returned.

### First Party API
The role of the first party API is to generate tokens for third party websites and validate these tokens from the user applications. In this PoC, there are only two endpoints (/generate and /verify).

The third party websites have to request a token, using the /generate endpoint, in order to access the client's application data. An API key is used to authenticate the requested. If valid, a signed JWT with a short life (to prevent re-use) and the requested information is returned. This token will then be transmitted to the local client.

The local client will then validate the token, using the /verify endpoint, and, if valid, will retrieve information about the requested.

### Local Client
The local client (built with Electron) runs a local web server, listening for incoming requests for its data. All requests need a token query parameter which is used to verify the requester. In order to do this, the token is transmitted to the first party API, and, if valid, will return the requester information.

This will then prompt a dialog to the user with the requester information. If the user grants permission for his data to be transmitted to the requester, the data will be transferred through the open HTTP connection (kept alive using event-stream). 

## Flow
Here's a diagram flow of a successful request
```mermaid
  sequenceDiagram
    participant TPW as Third Party Website
    participant FPA as First Party API
    participant LC as Local Client
    TPW->>+FPA: Request client access token
    FPA->>FPA: Check authorization
    FPA->>-TPW: Respond with signed token
    TPW->>+LC: Request client data using token
    LC->>+FPA: Verify validity of token
    FPA->>-LC: Return requester information
    LC->>LC: Prompt sharing consent dialog
    LC->>-TPW: Requested client data
```

## Notes
- This project doesn't include any tests as it is a proof of concept. But if we would want to implement tests for this functionality, we would be looking at end to end tests as it has multiple moving parts and it would make sense to test the entire flow.
- A config file for the API keys has been used rather than a database for the sake of simplicity. 
- Plain HTML and JS was used for the app and web part as implementing a full React app for a PoC would be overkill. The code from the PoC could easily be adapted to an existing React app (ie: Alert prompt would probably be a modal).

# Improvements
- Protect the information transmitted. We would need a valid certificate to enable HTTPS on the local server. That would involve adding a certificate to the user system keystore (which I'm not a big fan of) but we can do it differently. The request from the third party website (TPW) to the local client could include a public key (the TPW would hold the private key), and we could encrypt the data in the local client using this public key for our response. 
- Port rotation if already in use. There might be cases where the ports are already use and that would prevent the web server from starting correctly. This could be handled with a list of ports that rotate if some are busy. From the third party website, that would involve testing whether the local client is listening on a certain port before moving onto the next one.
- Better confirmation UI. As this is a PoC, the UI is far from ideal but still worth mentioning as an obvious improvement. It should clearly indicate **who** is trying to access **what** from **where**. Also the action buttons should have a small disabled period in order to prevent miss clicks. 
- Allow/Block lists for previously prompted websites. Along with the better confirmation UI, the user should have the possibility to permanently remember their choice for allowing or denying the access to their information.
- Notification for prompt. The client should notify the user (task bar blinking, bringing the application to the foreground) so that the user can approve the prompt immediately.
- Display & handle errors. The PoC just shows the working concept but in real life we want to display any errors to the user and also handle edge-cases that could cause this PoC to fail.
- Logging. All requests should be logged for auditing purposes. We can know who requested access to which client and when it happened. In case of a security incident, or simply for monitoring, this is useful information.
- Service resilience. This would require further investigation, but it would be ideal to have a way for the third party website to still be able to request data from a local client that previously allowed data access in case the first party API goes down.  

## An alternative design: Indirect Websocket communication
Another design that would not require running an HTTP server on the local client would be to have a Websocket connection from the client to a first party service and from the third party website to the first party service as well. But there are some complexities with this design:
- We need to know which local client made the request. This can be achieved by creating a cookie to identify the user on the first party API. This would require the user to authenticate on a service running on the same domain as the first party API. But if the user is not authenticated, we have to move him to the authentication flow.
- We need to make sure the information is routed correctly as we're "middle-manning" here. This additional layer has the important tasks to deliver the information to the right people. Not routing sensitive information (due to a bug or oversight) might have negative consequences.
- Maintaining a lot of concurrent websocket connections alive requires some horse power, this could end up being a costly design.
- The overhead of working with web sockets (making sure both, the local client and third party website, are connected in the same time, dealing with reconnection, etc...). Websockets are good for many cases but in this case, it adds more complexity than benefits.

## Things I learned
### Mixed content and localhost
In the recent years, with the push to secure websites, browsers have aggressively stared blocking non-secure resources. Although this change was necessary for making the web more secure, this also lead to a couple of issues (sometimes for the better, but not always...). Fortunately, it is now allowed to request 127.0.0.1. Although this was not the case initially, blocking non-secure requests to loopback addresses had a lot of side effects with no benefit for the user. This has been discussed on the [Chromium](https://chromium.googlesource.com/chromium/src.git/+/130ee686fa00b617bfc001ceb3bb49782da2cb4e) and [Mozilla](https://bugzilla.mozilla.org/show_bug.cgi?id=903966) bug trackers which resulted in the change for allowing localhost requests.

### WebRTC
WebRTC was an interesting candidate for this PoC as it offers the possibility to transmit data directly from peer to peer. Unfortunately, I have not found a way to establish a connection between local peers without signaling server. There might be a way but all examples I have encountered rely on a signaling server. Baking a signaling server into the Electron app would be overkill versus the design describer in the PoC (a simple HTTP server). So the idea was quickly abandoned without much further research.

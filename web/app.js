const path = require('path');
const fetch = require('node-fetch');
const express = require('express');
const app = express();

const { port, api, api_key } = require('./config.json');

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    fetch(`${api}/generate?key=${api_key}`)
        .then(res => res.text())
        .then(token => res.render('index', { token }))
        .catch(err => res.send(err)); // Obfuscate error for public consumption (& log it for us)
});

app.listen(port, () => console.log(`Web listening at http://localhost:${port}`));

# Third Party Website
Example of a third party website accessing information from the local client.

## Notes
In this demo, I am first requesting the token from the server before rendering it on the page using a template engine. This can also be done asynchronously from the front-end via an HTTP request to one of the third party's website endpoint.

## Running this project
- Install dependencies: `npm i`.
- Tweak config if needed (inside `config.json`).
- Run using `npm start`.

## Config
Here are the available configuration properties in the `config.json` file.

| Name | Type | Description | Example |
| ---- | ---- | ---- | ---- |
| port | number | Port this application will run on | 3002 |
| api | string | URL to the API | "http://localhost:3001" |
| api_key | string | API key for the API above | "50f6e3e554dc496256bacfb4651a7bbfd675e21c" |
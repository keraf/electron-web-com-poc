# API
Responsible of issuing tokens and validating them.

## Running this project
- Install dependencies: `npm i`.
- Tweak config if needed (inside `config.json`).
- Run using `npm start`.

## Endpoints
### /generate
Generates a token for an authorized client. The request should include the API key (see `config.json` and params below) and, if valid, will return a JWT that can be used to retrieve the data from the local client. All generated JWTs are short lived (5 minutes). 

**Params**
- key = API key

**Returns**
- JWT if successful.

### /verify
Endpoint to verify the validity of the token. 

**Params**
- token = Token to validate

**Returns**
- Requester information (from the JWT).

## Config
Here are the available configuration properties in the `config.json` file.

| Name | Type | Description | Example |
| ---- | ---- | ---- | ---- |
| port | number | Port this application will run on | 3001 |
| secret | string | Secret for signing the JWT | "verysecretsecret" |
| api_keys | array of *api_key* | Array containing all authorized api keys and information about who they belong to (see below for structure) | - |

### api_key
| Name | Type | Description | Example |
| ---- | ---- | ---- | ---- |
| key | string | API key | "50f6e3e554dc496256bacfb4651a7bbfd675e21c" |
| name | string | Name of the key owner | "Third Party Company" |
| website | string | Website of the key owner | "thirdpartywebsite.test" |

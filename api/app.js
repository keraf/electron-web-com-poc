const jwt = require('jsonwebtoken');
const express = require('express');
const app = express();

const {
    port,
    secret,
    api_keys,
} = require('./config.json');

/**
 * Generate an authorization token
 */
app.get('/generate', (req, res) => {
    const { key } = req.query;

    if (key === undefined) {
        // Missing API key
        return res.sendStatus(400);
    }

    const profile = api_keys.find(ak => ak.key === key);
    if (profile === undefined) {
        // Not allowed
        return res.sendStatus(401);
    }

    const token = jwt.sign({
        name: profile.name,
        website: profile.website,
    }, secret, { expiresIn: 5 * 60 /* 5min */ });

    return res.send(token);
});

/**
 * Verify that the token is valid and return information
 */
app.get('/verify', (req, res) => {
    const { token } = req.query;

    if (token === undefined) {
        // Missing token
        return res.sendStatus(400);
    }

    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            return res.sendStatus(400);
        }

        return res.send(decoded);
    });
});

app.listen(port, () => console.log(`API listening at http://localhost:${port}`));

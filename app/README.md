# App

## Running this project
- Install dependencies: `npm i`.
- Tweak config if needed (inside `config.json`).
- Run using `npm start`.

## Config
Here are the available configuration properties in the `config.json` file.

| Name | Type | Description | Example |
| ---- | ---- | ---- | ---- |
| port | number | Port for the web server in this application | 3206 |
| api | string | URL to the API | "http://localhost:3001" |
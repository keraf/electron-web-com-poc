
const http = require('http');
const crypto = require('crypto');
const querystring = require('querystring');
const fetch = require('node-fetch');
const {
    app,
    ipcMain,
    BrowserWindow,
} = require('electron');

const { port, api } = require('./config.json');

// Random key
const key = crypto.randomBytes(64).toString('hex');

let promptVisible = false;
let win = null;

const createWindow = () => {
    win = new BrowserWindow({
        width: 1200,
        height: 200,
        webPreferences: {
            nodeIntegration: true,
        },
    });

    win.loadFile('ui/index.html');

    win.webContents.once('dom-ready', () => {
        win.webContents.send('set-key', key);
    });
};

const createServer = () => {
    const requestListener = (req, res) => {
        res.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Access-Control-Allow-Origin': '*',
        });

        if (promptVisible) {
            return res.end();
        }

        const url = req.url.replace('/?', '');
        const { token } = querystring.parse(url);

        if (token === undefined) {
            return res.end();
        }

        fetch(`${api}/verify?token=${token}`)
            .then(res => res.json())
            .then(info => {
                win.webContents.send('ask-permission', info.name);
                promptVisible = true;

                ipcMain.once('permission-response', (event, arg) => {
                    promptVisible = false;

                    // If the confirmation was good, write the key in the response
                    if (arg) {
                        res.write(`data: ${key}\n\n`);
                    }

                    res.end();
                });
            })
            .catch(err => res.end());

    }
    
    const server = http.createServer(requestListener);
    server.listen(port);
};

app.whenReady()
    .then(createServer)
    .then(createWindow);

//////////
const getTokenFromUrl = url => url.replace('/?token=', '');

const { ipcRenderer } = require('electron');

ipcRenderer.on('set-key', (event, arg) => {
    document.getElementById('key').innerText = arg;
});

ipcRenderer.on('ask-permission', (event, arg) => {
    const response = confirm(`${arg} would like to access your key. Allow?`);
    ipcRenderer.send('permission-response', response);
});